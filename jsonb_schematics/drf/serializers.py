# coding: utf-8
import json

from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.serializers import OrderedDict, SkipField, PKOnlyObject, \
    empty
from schematics.models import Model as SchematicsModel

from jsonb_schematics.collections import dict_merge
from jsonb_schematics.db_fields import SchematicsValidation


__author__ = 'vadim'


class ContextSerializationMixin(object):
    update_context = None

    def to_representation(self, instance):
        """
        Object instance -> Dict of primitive datatypes.
        """
        ret = OrderedDict()
        fields = self._readable_fields

        update_context = getattr(self.Meta, 'update_context')

        if update_context:
            context = update_context(instance, self.context)
        else:
            context = self.context

        for field in fields:
            try:
                attribute = field.get_attribute(instance)
            except SkipField:
                continue

            # We skip `to_representation` for `None` values so that fields do
            # not have to explicitly deal with that case.
            #
            # For related fields with `use_pk_only_optimization` we need to
            # resolve the pk value.
            check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            if check_for_none is None:
                ret[field.field_name] = None
            else:
                try:
                    ret[field.field_name] = field.to_representation(attribute, context=context)
                except TypeError:
                    ret[field.field_name] = field.to_representation(attribute)
        return ret


class PartialSerializationMixin(object):
    def __init__(self, instance=None, data=empty, **kwargs):
        if hasattr(self.Meta, 'partial_fields') and kwargs.get('partial') and instance and isinstance(data, dict):
            for name in self.Meta.partial_fields:
                if name in data:
                    val = getattr(instance, name, {})
                    if isinstance(val, SchematicsModel):
                        val = val.to_primitive()
                    update_val = data.get(name)
                    dict_merge(val, update_val)
                    data[name] = val
        super().__init__(instance, data, **kwargs)


class BaseSerializationMixin(ContextSerializationMixin,
                             PartialSerializationMixin):
    """
    Serialization mixin collect same base mixin
    """
    pass


class SchematicsField(serializers.Field):
    default_error_messages = {
        'invalid': _('Value must be valid JSON.')
    }

    def __init__(self, schematics_model=None, *args, **kwargs):
        self.schematics_model = schematics_model
        super().__init__(*args, **kwargs)

    def to_internal_value(self, data):
        try:
            json.dumps(data)
            for validator in self.validators:
                if isinstance(validator, SchematicsValidation):
                    validator(data, strict=True)
            if self.schematics_model:
                validator = SchematicsValidation(self.schematics_model)
                validator(data, strict=True)
        except (TypeError, ValueError):
            self.fail('invalid')

        return data

    def to_representation(self, value, context=None):
        if isinstance(value, SchematicsModel):
            role = (context or {}).get('role')
            try:
                return value.to_primitive(role=role, raise_error_on_role=False)
            except TypeError:
                return value.to_primitive(role=role)
        return value
