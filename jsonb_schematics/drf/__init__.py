# coding: utf-8
import rest_framework.serializers

from jsonb_schematics.db_fields import SchematicsField as ModelSchematicsField
from jsonb_schematics.drf.serializers import SchematicsField

__author__ = 'vadim'


rest_framework.serializers.ModelSerializer.serializer_field_mapping[ModelSchematicsField] = SchematicsField
