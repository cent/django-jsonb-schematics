# coding: utf-8
import json

from django.utils.encoding import force_text
from rest_framework.response import Response
from rest_framework.views import exception_handler


__author__ = 'vadim'


def try_json(v):
    try:
        return json.loads(v)
    except Exception:
        return v


def try_loads_structure(v):
    if isinstance(v, list):
        v = [try_json(vv) for vv in v]
    elif isinstance(v, dict):
        v = {kk: try_json(vv) for kk, vv in v.items()}
    return v


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        if hasattr(response, 'data'):
            data = {
                'status_code': response.status_code,
            }
            if isinstance(response.data, dict):
                if response.data.get('detail', None):
                    data['message'] = response.data.pop('detail')

                data['errors'] = {k: try_loads_structure(v) for k, v in response.data.items()}

            elif isinstance(response.data, list):
                if len(response.data) and len(response.data) > 0:
                    data['message'] = response.data[0]

                    data['errors'] = [try_loads_structure(v) for v in response.data[1:]]

            if not data.get('errors'):
                del data['errors']

            response.data = data

    return response
