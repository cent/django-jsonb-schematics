# coding: utf-8
import json
import schematics
from django.contrib.postgres.fields import JSONField
from django.db import IntegrityError
from django.db.models import CharField, Expression
from django.utils.translation import ugettext_lazy as _
from django.core import exceptions
from django.utils import six
from schematics.models import Model as SchematicsModel
import schematics.exceptions


__author__ = 'vadim'


class Jsonb(Expression):
    def __init__(self, expression, output_field=None):
        self.expression = expression
        output_field = output_field or CharField
        internal_type = output_field().get_internal_type()
        if internal_type == 'DateTimeField':
            self.db_type = '::TIMESTAMP WITH TIME ZONE'
        elif internal_type == 'DateField':
            self.db_type = '::DATE'
        elif internal_type == 'FloatField':
            self.db_type = '::FLOAT'
        elif internal_type.endswith('IntegerField'):
            self.db_type = '::INTEGER'
        elif internal_type == 'DecimalField':
            self.db_type = '::NUMERIC'
        else:
            self.db_type = ''

        super(Jsonb, self).__init__(output_field=output_field())

    def as_sql(self, compiler, connection):
        return '(%s)%s' % (self.expression, self.db_type), []


class SchematicsValidation:
    def __init__(self, schematics_model):
        self.schematics_model = schematics_model

    def __call__(self, value, *args, **kwargs):
        try:
            self.schematics_model(value, *args, **kwargs).validate()
        except schematics.exceptions.BaseError as e:
            raise exceptions.ValidationError(json.dumps(e.to_primitive()))


class SchematicDescriptor(object):
    def __init__(self, field):
        self.field = field

    def __get__(self, instance, cls=None):
        if instance is None:
            return self

        if self.field.name in instance.__dict__:
            return instance.__dict__[self.field.name]

        return None

    def __set__(self, instance, value):
        if isinstance(value, six.string_types):
            value = json.loads(value)
        if isinstance(value, dict):
            value = self.field.schematics_model(value)
        instance.__dict__[self.field.name] = value


class SchematicsField(JSONField):
    description = _("Schematics field based JSONField")
    descriptor_class = SchematicDescriptor

    def __init__(self, schematics_model, verbose_name=None, name=None, **kwargs):
        self.schematics_model = schematics_model
        self.default_validators = [SchematicsValidation(self.schematics_model)]
        super(SchematicsField, self).__init__(verbose_name, name, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(SchematicsField, self).deconstruct()
        args.insert(0, self.schematics_model)
        return name, path, args, kwargs

    def from_db_value(self, value, expression, connection, context):
        return self.to_python(value)

    def to_python(self, value):
        if value is None:
            return value

        if isinstance(value, SchematicsModel):
            return value

        return self.schematics_model(value)

    def get_prep_value(self, value):
        if value is not None:
            if isinstance(value, SchematicsModel):
                try:
                    value.validate()
                except schematics.exceptions.BaseError as e:
                    raise IntegrityError(json.dumps(u'{}: {}'.format(self.name, e.to_primitive())))
                value = value.to_primitive()
            return super(SchematicsField, self).get_prep_value(value)
        return value

    def contribute_to_class(self, cls, name, **kwargs):
        super().contribute_to_class(cls, name, **kwargs)
        setattr(cls, self.name, self.descriptor_class(self))

    def value_to_string(self, obj):
        value = super().value_to_string(obj)
        return value.to_primitive() if value else value
